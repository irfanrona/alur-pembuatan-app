# Alur Pembuatan App

## BRD (Bussines Requirment Document)
Dibuat oleh Orang Bisnis, biasanya hasil dari orang Team product, Team bisnis, dan Team operation.
Lalu diberikan pada team Teknologi

- Aplikasi yang Dibuat
    - MVP Most Valuable Product
- Fitur
- Alur (Flowchart) Bisnis

## UI / UX
- Demo with Adobe XD or etc

## Techincal Design
- Bikin satu dokumen Confluence
1. Deployment Diagram
2. ERD

## Architecture Review
- Team Infra
- Security Architecture
- Dev Architecture
- Frontend Architecture

## API Specification
1. Backend App
2. Postman Collection ex: https://github.com/ProgrammerZamanNow/kotlin-restful-api

## Development
BE, FE, QA
-BE API
-FE Frontend
-QA QA Automation from API Spec

## NON Production Deployment
- CI/CD

## Testing
-End to End test (dibuat ketika QA Automation)
- Performance Testing
- Security Testing

## Product Deployment
- A/B Testing
- Canary Deployment
- etc

## Maintenance Improvement
Maintenance and Improvement
- create Monitoring System
-- Total Data
-- Total Traffic
-- Response Time
